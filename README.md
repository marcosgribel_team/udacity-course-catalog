# README #


### Get Started ###

* [Android Studio](https://developer.android.com/studio/index.html)
* [Java 1.8](http://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html)
* [Kotlin](https://kotlinlang.org/)
* [Configure Kotlin plugin on Android Studio](https://blog.jetbrains.com/kotlin/2013/08/working-with-kotlin-in-android-studio/)

#### Description ####

This project leverages on modern approaches to build Android applications, with architecture is based on [MVVM](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93viewmodel) and [Clean Architecture](https://8thlight.com/blog/uncle-bob/2012/08/13/the-clean-architecture.html) and powered by [Android Architecture Components](https://developer.android.com/topic/libraries/architecture/room).

- 100% written in Kotlin programming language.
- Including test at unit and integration test.
- Easy to modularize, following the feature by package strategy.
- Solid Reactive behavior with RxJava2 and LiveData.
- 100% online project.

This project is focused to illustrate a real scenario and how I usually organize, structure and develop the projects.


#### Libraries ####
- Retrofit 2.x
- Okhttp
- Dagger 2.x
- RxJava/RxAndroid 2.x
- Glide
- Architecture Components
- Timber
- Robolectric
- Mockito