package com.marcosgribel.udacity_course_catalog.instructor.view.ui

import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.marcosgribel.com.marcosgribel.udacity_course_catalog.R
import com.marcosgribel.udacity_course_catalog.core.model.domain.entities.Instructor
import com.marcosgribel.udacity_course_catalog.instructor.view.adapter.InstructorRecyclerAdapter
import kotlinx.android.synthetic.main.fragment_list_instructor.*
import java.util.*

/**
 * Created by marcosgribel on 6/28/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class ListInstructorFragment : Fragment() {


    companion object {

        val TAG = ListInstructorFragment::class.java.simpleName.toString()

        fun newInstance(context: Context, instructors: List<Instructor>): ListInstructorFragment {
            var args = Bundle()
            args.putParcelableArrayList(context.getString(R.string.args_list_instructor), instructors as ArrayList<out Parcelable>)

            var fragment = ListInstructorFragment()
            fragment.arguments = args
            return fragment
        }

    }

    private lateinit var instructors: List<Instructor>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        instructors = arguments?.getParcelableArrayList(getString(R.string.args_list_instructor))!!

        return inflater.inflate(R.layout.fragment_list_instructor, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var adapter = InstructorRecyclerAdapter(instructors) {onItemClick(it)}

        var layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        recycler_list_instructor.layoutManager = layoutManager
        recycler_list_instructor.adapter = adapter

    }

    private fun onItemClick(instructor: Instructor) {

        val bottomSheetDialog = DetailInstructorBottomSheetDialogFragment.newInstance(activity!!, instructor)
        bottomSheetDialog.show(fragmentManager, TAG)

    }

}