package com.marcosgribel.udacity_course_catalog.instructor.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.marcosgribel.com.marcosgribel.udacity_course_catalog.R
import com.marcosgribel.udacity_course_catalog.core.extension.loadUrl
import com.marcosgribel.udacity_course_catalog.core.extension.onClickListener
import com.marcosgribel.udacity_course_catalog.core.model.domain.entities.Instructor
import kotlinx.android.synthetic.main.item_instructor.view.*

/**
 * Created by marcosgribel on 6/28/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class InstructorRecyclerAdapter constructor(var items: List<Instructor>, private val onItemClick: (Instructor) -> Unit) : RecyclerView.Adapter<InstructorRecyclerAdapter.InstructorRecyclerViewHolder>() {

    inner class InstructorRecyclerViewHolder(var view: View) : RecyclerView.ViewHolder(view) {

        fun bindData(data: Instructor) {

            view.txt_name_instructor.text = data.name
            view.txt_bio_instructor.apply {
                text = data.bio
                maxLines = 3
            }
            data.image?.let {
                view.img_photo_instructor.loadUrl(it, R.drawable.ic_man)
            }
        }
    }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InstructorRecyclerViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_list_instructor, parent, false)
        val holder = InstructorRecyclerViewHolder(view)
        holder.onClickListener { position, _ ->
            onItemClick(items[position])
        }
        return holder
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: InstructorRecyclerViewHolder, position: Int) {
        val item = items[position]
        holder.bindData(item)
    }
}