package com.marcosgribel.udacity_course_catalog.instructor.view.ui

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.view.LayoutInflater
import com.marcosgribel.com.marcosgribel.udacity_course_catalog.R
import com.marcosgribel.udacity_course_catalog.core.extension.loadUrl
import com.marcosgribel.udacity_course_catalog.core.model.domain.entities.Instructor
import kotlinx.android.synthetic.main.item_instructor.view.*

/**
 * Created by marcosgribel on 6/28/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class DetailInstructorBottomSheetDialogFragment : BottomSheetDialogFragment() {

    companion object {

        val TAG = DetailInstructorBottomSheetDialogFragment::class.java.simpleName.toString()

        fun newInstance(context: Context, instructor: Instructor): DetailInstructorBottomSheetDialogFragment {
            var args = Bundle()
            args.putParcelable(context.getString(R.string.args_instructor), instructor)
            var fragment = DetailInstructorBottomSheetDialogFragment()
            fragment.arguments = args
            return fragment
        }

    }

    private val contentView by lazy {
        LayoutInflater.from(context).inflate(R.layout.bottom_sheet_detail_instructor, null)
    }
    private val instructor by lazy {
        arguments?.getParcelable(context?.getString(R.string.args_instructor)) as Instructor
    }

    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog?, style: Int) {
        super.setupDialog(dialog, style)
        dialog?.setContentView(contentView)

        instructor.image?.let {
            contentView.img_photo_instructor.loadUrl(it, R.drawable.ic_man)
        }

        contentView.txt_name_instructor.text = instructor.name
        contentView.txt_bio_instructor.text = instructor.bio
    }


}