package com.marcosgribel.udacity_course_catalog

import com.marcosgribel.com.marcosgribel.udacity_course_catalog.BuildConfig
import com.marcosgribel.udacity_course_catalog.core.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import timber.log.Timber

/**
 * Created by marcosgribel on 6/26/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class App : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().application(this).build()
    }


    override fun onCreate() {
        super.onCreate()
    }


    fun timberConfig() {
        if (BuildConfig.DEBUG)
            Timber.plant(Timber.DebugTree())
    }
}