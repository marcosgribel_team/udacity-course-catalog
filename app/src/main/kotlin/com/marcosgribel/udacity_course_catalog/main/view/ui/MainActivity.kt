package com.marcosgribel.udacity_course_catalog.main.view.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.marcosgribel.com.marcosgribel.udacity_course_catalog.R
import com.marcosgribel.udacity_course_catalog.course.view.ui.ListCourseActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startActivity(ListCourseActivity.newIntent(this))
        finish()
    }

}
