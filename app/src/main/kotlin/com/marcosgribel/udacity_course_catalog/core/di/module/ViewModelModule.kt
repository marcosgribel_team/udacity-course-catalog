package com.marcosgribel.udacity_course_catalog.core.di.module

import android.arch.lifecycle.ViewModelProvider
import com.marcosgribel.udacity_course_catalog.core.di.factory.ViewModelFactory
import dagger.Binds
import dagger.Module

/**
 * Created by marcosgribel on 6/26/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

}