package com.marcosgribel.udacity_course_catalog.core.di.module

import com.marcosgribel.udacity_course_catalog.core.model.service.endpoint.CourseApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

/**
 * Created by marcosgribel on 6/26/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
@Module
class ServiceModule {

    @Provides
    fun provideCourseApi(retrofit: Retrofit) : CourseApi = retrofit.create(CourseApi::class.java)


}