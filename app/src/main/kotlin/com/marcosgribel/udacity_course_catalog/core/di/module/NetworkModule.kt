package com.marcosgribel.udacity_course_catalog.core.di.module


import android.app.Application
import com.marcosgribel.com.marcosgribel.udacity_course_catalog.BuildConfig
import com.marcosgribel.com.marcosgribel.udacity_course_catalog.R
import com.marcosgribel.udacity_course_catalog.core.di.ApiBaseUrl
import com.marcosgribel.udacity_course_catalog.core.di.NetworkCache
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import dagger.Module
import dagger.Provides
import dagger.Reusable
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by marcosgribel on 6/26/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
@Module
class NetworkModule {

    private val MAX_AGE = 3600
    private val MAX_STALE = 3600

    @Provides
    @ApiBaseUrl
    internal fun provideApiBaseUrl(): String = BuildConfig.BASE_API_URL

    @Provides
    @Reusable
    internal fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        var httpLoggingInterceptorLevel = HttpLoggingInterceptor.Level.NONE
        if (BuildConfig.DEBUG)
            httpLoggingInterceptorLevel = HttpLoggingInterceptor.Level.BODY

        return HttpLoggingInterceptor().apply {
            level = httpLoggingInterceptorLevel
        }
    }


    @Provides
    @Reusable
    @NetworkCache
    internal fun provideCacheNetworkInterceptor(): Interceptor {
        return object : Interceptor {
            override fun intercept(chain: Interceptor.Chain?): Response {
                var response = chain?.proceed(chain.request())

                return response!!.newBuilder()
                        .header("Cache-Control", String.format("max-age=%d, only-if-cached, max-stale=%d", MAX_AGE, MAX_STALE))
                        .build()

            }
        }

    }

    @Provides
    @Reusable
    internal fun provideOkHttpClient(context: Application,
                                     httpLoggingInterceptor: HttpLoggingInterceptor,
                                     @NetworkCache networkCacheInterceptor: Interceptor): OkHttpClient {

        val httpCacheDirectory = File(context.cacheDir, context.getString(R.string.app_dir_cache_name))
        val cacheSize = 128L * 1024 * 1024
        val cache = Cache(httpCacheDirectory, cacheSize)

        return OkHttpClient()
                .newBuilder()
                .addInterceptor(httpLoggingInterceptor)
                .addNetworkInterceptor(networkCacheInterceptor)
                .readTimeout(45, TimeUnit.SECONDS)
                .connectTimeout(2, TimeUnit.SECONDS)
                .cache(cache)

                .build()

    }


    @Provides
    @Reusable
    internal fun provideMoshiConverterFactory(): MoshiConverterFactory {
        var moshi = Moshi.Builder()
        moshi.add(Date::class.java, Rfc3339DateJsonAdapter().nullSafe())
        return MoshiConverterFactory.create(moshi.build())
    }

    @Provides
    @Reusable
    internal fun provideRxJava2CallAdapterFactory(): RxJava2CallAdapterFactory = RxJava2CallAdapterFactory.create()


    @Provides
    @Reusable
    internal fun provideRetrofit(
            @ApiBaseUrl apiBaseUrl: String,
            moshiConverterFactory: MoshiConverterFactory,
            rxJava2CallAdapterFactory: RxJava2CallAdapterFactory,
            okHttpClient: OkHttpClient): Retrofit {

        return Retrofit.Builder()
                .baseUrl(apiBaseUrl)
                .addConverterFactory(moshiConverterFactory)
                .addCallAdapterFactory(rxJava2CallAdapterFactory)
                .client(okHttpClient)
                .build()
    }


}