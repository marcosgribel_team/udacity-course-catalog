package com.marcosgribel.udacity_course_catalog.core.view.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.marcosgribel.com.marcosgribel.udacity_course_catalog.R
import kotlinx.android.synthetic.main.fragment_error.view.*

/**
 * Created by marcosgribel on 6/27/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
interface ErrorFragmentCallBack {

    fun onTryAgainClickListener()

}

class ErrorFragment : Fragment() {

    companion object {

        val TAG = ErrorFragment::class.java.simpleName.toString()

        fun newInstance(): ErrorFragment {
            return ErrorFragment()
        }

    }

    private lateinit var callback: ErrorFragmentCallBack

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (activity is ErrorFragmentCallBack)
            callback = activity as ErrorFragmentCallBack
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_error, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.btn_try_again.setOnClickListener(onTryAgainClickListener())
    }


    fun onTryAgainClickListener(): View.OnClickListener {
        return View.OnClickListener { callback.onTryAgainClickListener() }
    }

}