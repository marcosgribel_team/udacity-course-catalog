package com.marcosgribel.udacity_course_catalog.core.view

import android.content.Context
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import com.marcosgribel.com.marcosgribel.udacity_course_catalog.R
import kotlinx.android.synthetic.main.view_course_level.view.*

/**
 * Created by marcosgribel on 6/26/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class LevelView : LinearLayout {
    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initialize()
    }


    private lateinit var viewLevel1: View
    private lateinit var viewLevel2: View
    private lateinit var viewLevel3: View

    private var color: Int = R.color.dark_gray


    var level: Int = 0
        set(value) {
            paint(value)
        }

    private fun initialize() {
        val view = View.inflate(context, R.layout.view_course_level, this)
        viewLevel1 = view.view_level_1
        viewLevel2 = view.view_level_2
        viewLevel3 = view.view_level_3

    }

    private fun paint(value: Int) {
        when (value) {
            1 -> setBeginner()
            2 -> setIntermediate()
            3 -> setAdvanced()
        }
    }


    private fun setBeginner() {
        viewLevel1.setBackgroundColor(ContextCompat.getColor(context, color))
    }

    private fun setIntermediate() {
        viewLevel1.setBackgroundColor(ContextCompat.getColor(context, color))
        viewLevel2.setBackgroundColor(ContextCompat.getColor(context, color))

    }

    private fun setAdvanced() {
        viewLevel1.setBackgroundColor(ContextCompat.getColor(context, color))
        viewLevel2.setBackgroundColor(ContextCompat.getColor(context, color))
        viewLevel3.setBackgroundColor(ContextCompat.getColor(context, color))
    }


}
