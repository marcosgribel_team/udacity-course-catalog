package com.marcosgribel.udacity_course_catalog.core.model.domain.entities

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Course(

        @Json(name = "syllabus")
        var syllabus: String? = null,

        @Json(name = "featured")
        var featured: Boolean? = null,

        @Json(name = "project_name")
        var projectName: String? = null,

        @Json(name = "title")
        var title: String? = null,

        @Json(name = "instructors")
        var instructors: List<Instructor>? = null,

        @Json(name = "faq")
        var faq: String? = null,

        @Json(name = "expected_duration_unit")
        var expectedDurationUnit: String? = null,

        @Json(name = "expected_duration")
        var expectedDuration: Int? = null,

        @Json(name = "required_knowledge")
        var requiredKnowledge: String? = null,

        @Json(name = "banner_image")
        var bannerImage: String? = null,

        @Json(name = "key")
        var key: String? = null,

        @Json(name = "short_summary")
        var shortSummary: String? = null,

        @Json(name = "slug")
        var slug: String? = null,

        @Json(name = "summary")
        var summary: String? = null,

        @Json(name = "image")
        var image: String? = null,

        @Json(name = "starter")
        var starter: Boolean? = null,

        @Json(name = "level")
        var level: String? = null,

        @Json(name = "project_description")
        var projectDescription: String? = null,

        @Json(name = "new_release")
        var newRelease: Boolean? = null,

        @Json(name = "subtitle")
        var subtitle: String? = null,

        @Json(name = "expected_learning")
        var expectedLearning: String? = null,

        @Json(name = "affiliates")
        var affiliates: List<Affiliate?>? = null,

        @Json(name = "full_course_available")
        var fullCourseAvailable: Boolean? = null,

        @Json(name = "homepage")
        var homepage: String? = null
) : Parcelable {

      var levelScore: Int = 0
                get() {
                        level.let {
                                return if(it.equals("Beginner", true))
                                        1
                                else if(it.equals("Intermediate", true))
                                        2
                                else if(it.equals("Advanced", true))
                                        3
                                else 0
                        }
                }
}