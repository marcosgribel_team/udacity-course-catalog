package com.marcosgribel.udacity_course_catalog.core.rx

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by marcosgribel on 6/21/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
class AppScheduler : ScheduleProvider {

    override fun mainThread(): Scheduler = AndroidSchedulers.mainThread()

    override fun trampoline(): Scheduler = Schedulers.trampoline()

    override fun io(): Scheduler = Schedulers.io()

    override fun newThread(): Scheduler = Schedulers.newThread()
}