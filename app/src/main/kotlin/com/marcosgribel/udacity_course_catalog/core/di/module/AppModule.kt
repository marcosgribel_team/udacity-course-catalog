package com.marcosgribel.udacity_course_catalog.core.di.module

import com.marcosgribel.udacity_course_catalog.core.di.AppSchedulerProvider
import com.marcosgribel.udacity_course_catalog.core.rx.AppScheduler
import com.marcosgribel.udacity_course_catalog.core.rx.ScheduleProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by marcosgribel on 6/26/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
@Module(includes = [
    NetworkModule::class,
    ServiceModule::class
])
class AppModule {

    @Provides
    @Singleton
    @AppSchedulerProvider
    fun provideScheduler(): ScheduleProvider = AppScheduler()

}