package com.marcosgribel.udacity_course_catalog.core.di

import javax.inject.Qualifier

/**
 * Created by marcosgribel on 6/26/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
@Qualifier
annotation class ApiBaseUrl

@Qualifier
annotation class AppSchedulerProvider

@Qualifier
annotation class TestSchedulerProvider

@Qualifier
annotation class NetworkCache