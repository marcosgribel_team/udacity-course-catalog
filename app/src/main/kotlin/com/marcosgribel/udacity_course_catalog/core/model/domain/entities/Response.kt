package com.marcosgribel.udacity_course_catalog.core.model.domain.entities

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Response(

        @Json(name = "courses")
        val courses: List<Course> = arrayListOf()

) : Parcelable