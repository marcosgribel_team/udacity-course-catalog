package com.marcosgribel.udacity_course_catalog.core.model.domain.enumerator

/**
 * Created by marcosgribel on 6/27/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}