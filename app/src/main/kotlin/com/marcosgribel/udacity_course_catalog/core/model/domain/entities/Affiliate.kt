package com.marcosgribel.udacity_course_catalog.core.model.domain.entities

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Affiliate(

        @Json(name = "image")
        val image: String? = null,

        @Json(name = "name")
        val name: String? = null

) : Parcelable