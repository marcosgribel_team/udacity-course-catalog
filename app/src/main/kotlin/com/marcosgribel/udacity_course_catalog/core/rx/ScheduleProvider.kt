package com.marcosgribel.udacity_course_catalog.core.rx

import io.reactivex.Scheduler

/**
 * Created by marcosgribel on 6/21/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
interface ScheduleProvider {

    fun mainThread(): Scheduler

    fun trampoline(): Scheduler

    fun io(): Scheduler

    fun newThread(): Scheduler

}

