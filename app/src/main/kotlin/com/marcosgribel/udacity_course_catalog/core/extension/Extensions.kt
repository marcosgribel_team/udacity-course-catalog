package com.marcosgribel.udacity_course_catalog.core.extension

import android.support.v7.widget.RecyclerView
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

/**
 * Created by marcosgribel on 6/27/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */


fun ImageView.loadUrl(url: String, placeHolder: Int) {

    var options = RequestOptions()
    options.placeholder(placeHolder)
    Glide.with(context)
            .load(url)
            .apply(options)
            .into(this)
}


/*
    Extension to add click listener to ViewHolder
 */
fun <T : RecyclerView.ViewHolder> T.onClickListener(event: (position: Int, type: Int) -> Unit): T {
    itemView.setOnClickListener {
        event.invoke(adapterPosition, itemViewType)
    }
    return this
}