package com.marcosgribel.udacity_course_catalog.course.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.marcosgribel.udacity_course_catalog.core.di.AppSchedulerProvider
import com.marcosgribel.udacity_course_catalog.core.model.domain.entities.Course
import com.marcosgribel.udacity_course_catalog.core.model.domain.enumerator.Status
import com.marcosgribel.udacity_course_catalog.core.rx.ScheduleProvider
import com.marcosgribel.udacity_course_catalog.course.model.usecase.CourseUseCase
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber

/**
 * Created by marcosgribel on 6/26/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class ListCourseViewModelFactory constructor(private val useCase: CourseUseCase,
                                             @AppSchedulerProvider private val scheduler: ScheduleProvider
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ListCourseViewModel::class.java)) {
            return ListCourseViewModel(useCase, scheduler) as T
        }
        throw RuntimeException("Unknown ViewModel class")
    }
}

class ListCourseViewModel constructor(
        private val useCase: CourseUseCase,
        private val scheduler: ScheduleProvider
) : ViewModel() {

    init {
        val TAG = ListCourseViewModel::class.java.simpleName.toString()
        Timber.tag(TAG)
    }

    private var compositeDisposable = CompositeDisposable()

    var courses: MutableLiveData<List<Course>> = MutableLiveData()
    var status: MutableLiveData<Status> = MutableLiveData()


    fun loadData() {

        status.value = Status.LOADING

        val disposable = useCase.getAll()
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.mainThread())
                .subscribe({
                    handleLoadDataSuccess(it)
                }, {
                    handleLoadDataException(it)
                })

        compositeDisposable.add(disposable)

    }

    fun handleLoadDataException(throwable: Throwable) {
        Timber.e(throwable)
        status.value = Status.ERROR
    }

    fun handleLoadDataSuccess(items: List<Course>) {
        status.value = Status.SUCCESS
        courses.value = items
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }

}