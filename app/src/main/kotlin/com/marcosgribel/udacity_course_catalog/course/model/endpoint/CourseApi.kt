package com.marcosgribel.udacity_course_catalog.core.model.service.endpoint

import com.marcosgribel.udacity_course_catalog.core.model.domain.entities.Response
import io.reactivex.Single
import retrofit2.http.GET

/**
 * Created by marcosgribel on 6/26/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
interface CourseApi {

    @GET("v0/courses")
    fun getCourseResponse() : Single<Response>

}