package com.marcosgribel.udacity_course_catalog.course.view.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.marcosgribel.com.marcosgribel.udacity_course_catalog.R
import com.marcosgribel.udacity_course_catalog.core.model.domain.entities.Course
import com.marcosgribel.udacity_course_catalog.core.model.domain.enumerator.Status
import com.marcosgribel.udacity_course_catalog.core.view.ui.ErrorFragment
import com.marcosgribel.udacity_course_catalog.core.view.ui.ErrorFragmentCallBack
import com.marcosgribel.udacity_course_catalog.course.view.adapter.CourseRecyclerAdapter
import com.marcosgribel.udacity_course_catalog.course.viewmodel.ListCourseViewModel
import com.marcosgribel.udacity_course_catalog.course.viewmodel.ListCourseViewModelFactory
import dagger.android.AndroidInjection
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_list_course.*
import javax.inject.Inject

/**
 * Created by marcosgribel on 6/26/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class ListCourseActivity : DaggerAppCompatActivity(), ErrorFragmentCallBack {

    @Inject
    lateinit var viewModelFactory: ListCourseViewModelFactory
    private lateinit var viewModel: ListCourseViewModel


    private lateinit var adapter: CourseRecyclerAdapter


    companion object {

        fun newIntent(context: Context): Intent {
            var intent = Intent(context, ListCourseActivity::class.java)
            return intent
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_course)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ListCourseViewModel::class.java)

        viewModel.courses.observe(this, observerListCourse())
        viewModel.status.observe(this, observerStatus())

        configRecycler()
    }


    private fun configRecycler() {
        adapter = CourseRecyclerAdapter { onItemRecyclerClick(it) }
        recycler_list_course.adapter = adapter
        recycler_list_course.setHasFixedSize(true)
        recycler_list_course.layoutManager = LinearLayoutManager(baseContext)
    }


    override fun onResume() {
        super.onResume()
        loadData()
    }


    private fun observerListCourse(): Observer<List<Course>> {
        return Observer {
            adapter.addAll(it!!)
        }
    }

    private fun observerStatus(): Observer<Status> {
        return Observer {

            when (it) {
                Status.LOADING -> {
                    handleLoading()
                }

                Status.ERROR -> {
                    handleGlobalError()
                }

                Status.SUCCESS -> {
                    handleSuccess()
                }
            }
        }
    }

    fun loadData() {
        viewModel.loadData()
    }

    fun handleLoading() {
        frame_content.visibility = View.GONE
        recycler_list_course.visibility = View.GONE
        progress_bar.visibility = View.VISIBLE
    }

    fun handleSuccess() {
        frame_content.visibility = View.GONE
        progress_bar.visibility = View.GONE
        recycler_list_course.visibility = View.VISIBLE
    }

    fun handleGlobalError() {
        recycler_list_course.visibility = View.GONE
        progress_bar.visibility = View.GONE
        frame_content.visibility = View.VISIBLE

        var errorFragment = ErrorFragment.newInstance()
        supportFragmentManager.beginTransaction().replace(
                R.id.frame_content, errorFragment, ErrorFragment.TAG)
                .commit()

    }

    private fun onItemRecyclerClick(course: Course) {
        startActivity(DetailCourseActivity.newIntent(this, course))
    }

    override fun onTryAgainClickListener() {
        loadData()
    }
}