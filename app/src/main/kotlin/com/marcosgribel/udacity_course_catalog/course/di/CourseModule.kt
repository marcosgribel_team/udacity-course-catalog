package com.marcosgribel.udacity_course_catalog.course.di

import com.marcosgribel.udacity_course_catalog.course.model.usecase.CourseUseCase
import com.marcosgribel.udacity_course_catalog.course.model.usecase.CourseUseCaseImpl
import dagger.Module
import dagger.Provides

/**
 * Created by marcosgribel on 6/26/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
@Module
class CourseModule {

    @Provides
    fun provideCourseUseCase(useCase: CourseUseCaseImpl): CourseUseCase = useCase

}