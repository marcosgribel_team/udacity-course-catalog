package com.marcosgribel.udacity_course_catalog.course.model.usecase

import com.marcosgribel.udacity_course_catalog.core.model.domain.entities.Course
import com.marcosgribel.udacity_course_catalog.core.model.service.endpoint.CourseApi
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by marcosgribel on 6/26/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
interface CourseUseCase {

    fun getAll(): Single<List<Course>>
}

@Singleton
class CourseUseCaseImpl @Inject constructor(val courseApi: CourseApi) : CourseUseCase {

    override fun getAll(): Single<List<Course>> {
        return courseApi.getCourseResponse().map {
            it.courses
        }
    }
}