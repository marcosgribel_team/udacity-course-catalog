package com.marcosgribel.udacity_course_catalog.course.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.marcosgribel.com.marcosgribel.udacity_course_catalog.R
import com.marcosgribel.udacity_course_catalog.core.extension.loadUrl
import com.marcosgribel.udacity_course_catalog.core.extension.onClickListener
import com.marcosgribel.udacity_course_catalog.core.model.domain.entities.Course
import kotlinx.android.synthetic.main.item_list_course.view.*

/**
 * Created by marcosgribel on 6/26/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class CourseRecyclerAdapter(private var onItemClickListener: (course: Course) -> Unit) : RecyclerView.Adapter<CourseRecyclerAdapter.CourseRecyclerViewHolder>() {


    inner class CourseRecyclerViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        fun bindData(data: Course) {

            view.txt_title.text = data.title
            view.txt_summary.text = data.summary
            view.view_level.level = data.levelScore

            view.view_level.visibility = View.GONE
            view.txt_level.visibility = View.GONE

            view.txt_level.text = data.level?.let {
                view.view_level.visibility = View.VISIBLE
                view.txt_level.visibility = View.VISIBLE
                it
            }

            data.image?.let {
                view.img_header.loadUrl(it, R.drawable.ic_launcher_background)
            }

        }

    }

    private var items: MutableList<Course> = arrayListOf()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CourseRecyclerViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_list_course, parent, false)
        val holder = CourseRecyclerViewHolder(view)

        holder.onClickListener { position, _ ->
            onItemClickListener(items[position])
        }

        return holder
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: CourseRecyclerViewHolder, position: Int) {
        val data = items[position]
        holder.bindData(data)
    }

    fun addAll(values: List<Course>) {
        items.addAll(values)
        notifyDataSetChanged()
    }
}