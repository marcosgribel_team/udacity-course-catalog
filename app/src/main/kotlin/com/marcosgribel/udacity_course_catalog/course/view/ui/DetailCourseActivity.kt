package com.marcosgribel.udacity_course_catalog.course.view.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import com.marcosgribel.com.marcosgribel.udacity_course_catalog.R
import com.marcosgribel.udacity_course_catalog.core.extension.loadUrl
import com.marcosgribel.udacity_course_catalog.core.model.domain.entities.Course
import com.marcosgribel.udacity_course_catalog.core.model.domain.entities.Instructor
import com.marcosgribel.udacity_course_catalog.instructor.view.ui.ListInstructorFragment
import kotlinx.android.synthetic.main.activity_detail_course.*
import kotlinx.android.synthetic.main.content_detail_course.*

/**
 * Created by marcosgribel on 6/28/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class DetailCourseActivity : AppCompatActivity() {

    companion object {

        fun newIntent(context: Context, course: Course): Intent {
            var intent = Intent(context, DetailCourseActivity::class.java)
            intent.putExtra(context.getString(R.string.args_course), course)
            return intent
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_course)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val course = intent.getParcelableExtra(getString(R.string.args_course)) as Course

        initialize(course)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                super.onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun initialize(data: Course) {

        txt_title_course_detail.text = data.title
        txt_summary_course_detail.text = data.summary

        view_level.level = data.levelScore

        view_level.visibility = View.GONE
        txt_level.visibility = View.GONE

        txt_level.text = data.level?.let {
            view_level.visibility = View.VISIBLE
            txt_level.visibility = View.VISIBLE
            it
        }

        data.image?.let {
            img_header_course_detail.loadUrl(it, R.drawable.ic_launcher_background)
        }

        data.instructors?.let {
            addListInstructorFragment(it)

        }

    }


    private fun addListInstructorFragment(instructors: List<Instructor>) {

        val fragment = ListInstructorFragment.newInstance(baseContext, instructors)
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.frame_content_instructors, fragment, ListInstructorFragment.TAG)
                .commit()

        card_content_instructors.visibility = View.VISIBLE

    }
}
