package com.marcosgribel.udacity_course_catalog.course.di

import com.marcosgribel.udacity_course_catalog.core.di.AppSchedulerProvider
import com.marcosgribel.udacity_course_catalog.core.rx.ScheduleProvider
import com.marcosgribel.udacity_course_catalog.course.model.usecase.CourseUseCase
import com.marcosgribel.udacity_course_catalog.course.viewmodel.ListCourseViewModelFactory
import dagger.Module
import dagger.Provides

/**
 * Created by marcosgribel on 6/28/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
@Module( includes = [CourseModule::class])
class ListCourseActivityModule {

    @Provides
    fun provideListCourseViewModelFactory(useCase: CourseUseCase,
                                          @AppSchedulerProvider scheduler: ScheduleProvider
    ): ListCourseViewModelFactory = ListCourseViewModelFactory(useCase, scheduler)
}