package com.marcosgribel.udacity_course_catalog

import com.marcosgribel.udacity_course_catalog.core.model.domain.entities.Course
import com.marcosgribel.udacity_course_catalog.core.model.domain.entities.Response
import com.squareup.moshi.Moshi
import io.reactivex.Single
import java.io.ByteArrayOutputStream
import java.io.InputStream

/**
 * Created by marcosgribel on 6/26/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
object AppMockHelper {

    private fun readTextStream(inputStream: InputStream): String {
        var result = ByteArrayOutputStream()
        var buffer = ByteArray(1024)
        var length = inputStream.read(buffer)

        while (length != -1) {
            result.write(buffer, 0, length)
            length = inputStream.read(buffer)
        }
        return result.toString("UTF-8");
    }

    private fun fakeCourseResponse(): Response {
        val inputStream = this::class.java.classLoader.getResourceAsStream("courses.json")
        val strJson = readTextStream(inputStream)
        val adapter = Moshi.Builder().build().adapter(Response::class.java)

        return adapter.fromJson(strJson)!!
    }

    fun fakeCourseResponseSingle(): Single<Response> {
        return Single.create {
            it.onSuccess(fakeCourseResponse())
        }
    }

    fun fakeListCourse() : List<Course> {
        return fakeCourseResponse().courses
    }

    fun fakeCourse(): Course {
        val inputStream = this::class.java.classLoader.getResourceAsStream("course.json")
        val strJson = readTextStream(inputStream)
        val adapter = Moshi.Builder().build().adapter(Course::class.java)

        return adapter.fromJson(strJson)!!

    }
}
