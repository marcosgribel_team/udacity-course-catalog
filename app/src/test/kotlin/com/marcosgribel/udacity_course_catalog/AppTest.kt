package com.marcosgribel.udacity_course_catalog

import com.marcosgribel.udacity_course_catalog.core.di.DaggerAppComponentTest
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

/**
 * Created by marcosgribel on 6/26/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class AppTest : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponentTest.builder().application(this).build()
    }

}