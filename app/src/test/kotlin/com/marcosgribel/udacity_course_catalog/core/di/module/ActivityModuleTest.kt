package com.marcosgribel.udacity_course_catalog.core.di.module

import com.marcosgribel.udacity_course_catalog.course.di.CourseModuleTest
import com.marcosgribel.udacity_course_catalog.course.view.ui.ListCourseActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by marcosgribel on 6/26/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
@Module
abstract class ActivityModuleTest {

    @ContributesAndroidInjector(modules = [CourseModuleTest::class])
    internal abstract fun bindListCourseActivity(): ListCourseActivity
}