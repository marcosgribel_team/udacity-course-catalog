package com.marcosgribel.udacity_course_catalog.core.di

import android.app.Application
import com.marcosgribel.udacity_course_catalog.AppTest
import com.marcosgribel.udacity_course_catalog.core.di.module.ActivityModuleTest
import com.marcosgribel.udacity_course_catalog.core.di.module.AppModuleTest
import com.marcosgribel.udacity_course_catalog.core.di.module.FragmentModuleTest
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created by marcosgribel on 6/26/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    AppModuleTest::class,
    ActivityModuleTest::class,
    FragmentModuleTest::class
])
interface AppComponentTest : AndroidInjector<AppTest> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponentTest

    }

}