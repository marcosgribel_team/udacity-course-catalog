package com.marcosgribel.udacity_course_catalog.course.view.ui

import android.content.Intent
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import com.marcosgribel.com.marcosgribel.udacity_course_catalog.BuildConfig
import com.marcosgribel.com.marcosgribel.udacity_course_catalog.R
import com.marcosgribel.udacity_course_catalog.AppMockHelper
import com.marcosgribel.udacity_course_catalog.AppTest
import com.marcosgribel.udacity_course_catalog.core.view.LevelView
import com.marcosgribel.udacity_course_catalog.instructor.view.ui.ListInstructorFragment
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config

/**
 * Created by marcosgribel on 6/28/18.
 *
 *
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 */
@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class, application = AppTest::class)
class DetailCourseActivityTest {

    private lateinit var detailCourseActivity: DetailCourseActivity

    private lateinit var viewTitle: TextView
    private lateinit var viewSummary: TextView
    private lateinit var viewLevel: LevelView
    private lateinit var viewLevelDescription: TextView
    private lateinit var frameLayout: FrameLayout

    @Before
    fun setUp() {

        var intent = Intent(RuntimeEnvironment.application, DetailCourseActivity::class.java)
        intent.putExtra(RuntimeEnvironment.application.getString(R.string.args_course), AppMockHelper.fakeCourse())

        detailCourseActivity = Robolectric.buildActivity(DetailCourseActivity::class.java, intent).create().get()


        viewTitle = detailCourseActivity.findViewById(R.id.txt_title_course_detail)
        viewSummary = detailCourseActivity.findViewById(R.id.txt_summary_course_detail)
        viewLevel = detailCourseActivity.findViewById(R.id.view_level)
        viewLevelDescription = detailCourseActivity.findViewById(R.id.txt_level)
        frameLayout = detailCourseActivity.findViewById(R.id.frame_content_instructors)
    }


    @Test
    fun assertElementOnScreen() {

        assertNotNull(viewTitle)
        assertNotNull(viewSummary)
        assertNotNull(viewLevel)
        assertNotNull(viewLevelDescription)
        assertNotNull(frameLayout)

    }

    @Test
    fun assertLevelGone() {

        var course = AppMockHelper.fakeCourse()
        course.level = null

        detailCourseActivity.initialize(course)
        assertEquals(View.GONE, viewLevel.visibility)
        assertEquals(View.GONE, viewLevelDescription.visibility)

    }

    @Test
    fun assertLevelVisible() {

        var course = AppMockHelper.fakeCourse()
        course.level = "beginner"

        detailCourseActivity.initialize(course)
        assertEquals(View.VISIBLE, viewLevel.visibility)
        assertEquals(View.VISIBLE, viewLevelDescription.visibility)

    }

    @Test
    fun assertInstructorIsShowed() {
        var course = AppMockHelper.fakeCourse()
        detailCourseActivity.initialize(course)

        assertEquals(View.VISIBLE, frameLayout.visibility)

        val fragment = detailCourseActivity.supportFragmentManager.findFragmentByTag(ListInstructorFragment.TAG)
        assertNotNull(fragment)
        assert(fragment.isAdded)

    }

    @Test
    fun assertInstructorIsHide() {
        var course = AppMockHelper.fakeCourse()
        course.instructors = null
        detailCourseActivity.initialize(course)
        assertEquals(View.VISIBLE, frameLayout.visibility)
    }
}