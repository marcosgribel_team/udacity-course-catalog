package com.marcosgribel.udacity_course_catalog.course.viewmodel

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.marcosgribel.udacity_course_catalog.AppMockHelper
import com.marcosgribel.udacity_course_catalog.core.model.domain.enumerator.Status
import com.marcosgribel.udacity_course_catalog.core.rx.TestScheduler
import com.marcosgribel.udacity_course_catalog.course.di.CourseModuleTest
import com.marcosgribel.udacity_course_catalog.course.model.usecase.CourseUseCase
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mockito


/**
 * Created by marcosgribel on 6/27/18.
 *
 *
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 */
class ListCourseViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private lateinit var mockUseCase: CourseUseCase
    private lateinit var mockViewModel: ListCourseViewModel


    @Before
    fun setUp() {
        mockUseCase = CourseModuleTest().provideCourseUseCase()
        mockViewModel = ListCourseViewModel(mockUseCase, TestScheduler())
    }

    @Test
    fun test_handleLoadDataSuccess() {
        val mockList = AppMockHelper.fakeListCourse()
        mockViewModel.handleLoadDataSuccess(mockList)
        Assert.assertEquals(Status.SUCCESS, mockViewModel.status.value)
        Assert.assertEquals(5, mockViewModel.courses.value?.size)
    }

    @Test
    fun test_handleLoadDataException() {
        mockViewModel.handleLoadDataException(Mockito.any())
        Assert.assertEquals(Status.ERROR, mockViewModel.status.value)
    }


}