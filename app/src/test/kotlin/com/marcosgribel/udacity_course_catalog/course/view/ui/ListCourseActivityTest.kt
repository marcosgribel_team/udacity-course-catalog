package com.marcosgribel.udacity_course_catalog.course.view.ui

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.FrameLayout
import android.widget.ProgressBar
import com.marcosgribel.com.marcosgribel.udacity_course_catalog.BuildConfig
import com.marcosgribel.com.marcosgribel.udacity_course_catalog.R
import com.marcosgribel.udacity_course_catalog.AppTest
import com.marcosgribel.udacity_course_catalog.core.view.ui.ErrorFragment
import com.marcosgribel.udacity_course_catalog.course.view.adapter.CourseRecyclerAdapter
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import org.hamcrest.Matchers
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.Shadows.shadowOf
import org.robolectric.annotation.Config

/**
 * Created by marcosgribel on 6/28/18.
 *
 *
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 */
@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class, application = AppTest::class)
class ListCourseActivityTest {


    private lateinit var listCourseActivity: ListCourseActivity

    private lateinit var recyclerView: RecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var frameLayout: FrameLayout

    @Before
    fun setUp() {
        listCourseActivity = Robolectric.setupActivity(ListCourseActivity::class.java)

        recyclerView = listCourseActivity.findViewById(R.id.recycler_list_course)
        progressBar = listCourseActivity.findViewById(R.id.progress_bar)
        frameLayout = listCourseActivity.findViewById(R.id.frame_content)
    }


    @Test
    fun assertElementsOnScree() {

        assertNotNull(recyclerView)
        assertNotNull(progressBar)
        assertNotNull(frameLayout)

    }

    @Test
    fun test_OnItemClickedAndOpenDetail() {

        val recycler = listCourseActivity.findViewById<RecyclerView>(R.id.recycler_list_course)
        val viewHolder = recycler.findViewHolderForAdapterPosition(0) as CourseRecyclerAdapter.CourseRecyclerViewHolder

        val adapter = recycler.adapter

        assert(adapter.itemCount > 0)


        viewHolder.itemView.performClick()
        val shadowActivity = shadowOf(listCourseActivity)
        val startedIntent = shadowActivity.nextStartedActivity
        val shadowIntent = shadowOf(startedIntent)
        assertThat(shadowIntent.intentClass.name, Matchers.equalTo(DetailCourseActivity::class.java.name))

    }

    @Test
    fun test_handleSuccessBehavior() {
        listCourseActivity.handleSuccess()

        assertEquals(View.VISIBLE, recyclerView.visibility)
        assertEquals(View.GONE, progressBar.visibility)
        assertEquals(View.GONE, frameLayout.visibility)

    }

    @Test
    fun test_handleLoadingBehavior() {
        listCourseActivity.handleLoading()

        assertEquals(View.GONE, recyclerView.visibility)
        assertEquals(View.VISIBLE, progressBar.visibility)
        assertEquals(View.GONE, frameLayout.visibility)
    }

    @Test
    fun test_handleGlobalErrorBehavior(){
        listCourseActivity.handleGlobalError()

        assertEquals(View.GONE, recyclerView.visibility)
        assertEquals(View.GONE, progressBar.visibility)
        assertEquals(View.VISIBLE, frameLayout.visibility)

        val fragmentError = listCourseActivity.supportFragmentManager.findFragmentByTag(ErrorFragment.TAG)
        assertNotNull(fragmentError)
        assert(fragmentError.isAdded)


    }
}