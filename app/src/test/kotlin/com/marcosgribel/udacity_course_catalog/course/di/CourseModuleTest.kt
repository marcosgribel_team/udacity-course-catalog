package com.marcosgribel.udacity_course_catalog.course.di

import com.marcosgribel.udacity_course_catalog.AppMockHelper
import com.marcosgribel.udacity_course_catalog.core.di.TestSchedulerProvider
import com.marcosgribel.udacity_course_catalog.core.model.service.endpoint.CourseApi
import com.marcosgribel.udacity_course_catalog.core.rx.ScheduleProvider
import com.marcosgribel.udacity_course_catalog.course.model.usecase.CourseUseCase
import com.marcosgribel.udacity_course_catalog.course.model.usecase.CourseUseCaseImpl
import com.marcosgribel.udacity_course_catalog.course.viewmodel.ListCourseViewModelFactory
import dagger.Module
import dagger.Provides
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`

/**
 * Created by marcosgribel on 6/26/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
@Module
class CourseModuleTest {

    @Provides
    fun provideCourseUseCase(): CourseUseCase {
        val mockCourseApi: CourseApi = Mockito.mock(CourseApi::class.java)

        `when`(mockCourseApi.getCourseResponse())
                .thenReturn(AppMockHelper.fakeCourseResponseSingle())

        return CourseUseCaseImpl(mockCourseApi)

    }

    @Provides
    fun provideListCourseViewModelFactory(useCase: CourseUseCase,
                                          @TestSchedulerProvider scheduler: ScheduleProvider
    ): ListCourseViewModelFactory = ListCourseViewModelFactory(useCase, scheduler)


    @Test
    fun test_getAllCourse_with_success() {
        val useCase = provideCourseUseCase()
        useCase.getAll().test().assertValue {
            it.size == 5
        }
    }
}